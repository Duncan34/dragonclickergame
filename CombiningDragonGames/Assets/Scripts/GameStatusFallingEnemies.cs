﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameStatusFallingEnemies : MonoBehaviour
{

    public int points = 0;
    public TextMeshProUGUI score = null;
    public GameObject gameOverPanel;
    public bool gameOver = false;
    public float speed = 1.0f;

    SharedVariablesAcrossScenes mySharedVariables = null;

    private void Awake()
    {
        gameOver = false;
        mySharedVariables = FindObjectOfType<SharedVariablesAcrossScenes>();
    }

    public void addScore()
    {
        points++;
        score.text = points.ToString();
    }

    public void gameOverTextAppear()
    {
        if (gameOver == false)
        {
            mySharedVariables.addDefense(points);
            gameOver = true;
            gameOverPanel.SetActive(true);
        }
    }

}
