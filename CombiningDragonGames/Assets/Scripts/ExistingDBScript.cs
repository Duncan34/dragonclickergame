﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ExistingDBScript : MonoBehaviour
{

    public Text DebugText;

    // Use this for initialization
    void Start()
    {
        var ds = new DataService("existing.db");
        //ds.CreateDB ();
        var dragons = ds.GetDragons();
        ToConsole(dragons);

        //dragons = ds.GetFireTypeDragons();
        //ToConsole("Searching for Fire type ...");
        //ToConsole(dragons);

        ////ds.CreateDragon();
        //ToConsole("New Dragon has been created");
        //var p = ds.GetOneWaterTypeDragon();
        //ToConsole(p.ToString());

    }

    private void ToConsole(IEnumerable<Dragon> dragons)
    {
        foreach (var Dragon in dragons)
        {
            ToConsole(Dragon.ToString());
        }
    }

    private void ToConsole(string msg)
    {
        DebugText.text += System.Environment.NewLine + msg;
        Debug.Log(msg);
    }

}