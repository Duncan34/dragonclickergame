﻿using SQLite4Unity3d;

public class Dragon
{

    public Dragon()
    {

    }

    public Dragon(int identity, string t, int l, int a, int d, int s, float e, float mE)
    {
        Id = identity;
        Type = t;
        Level = l;
        Attack = a;
        Defense = d;
        Speed = s;
        Experience = e;
        MaximumExperience = mE;
    }

    public Dragon(string t, int l, int a, int d, int s, float e, float mE)
    {
        Type = t;
        Level = l;
        Attack = a;
        Defense = d;
        Speed = s;
        Experience = e;
        MaximumExperience = mE;
    }

    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public string Type { get; set; }
    public int Level { get; set; }
    public int Attack { get; set; }
    public int Defense { get; set; }
    public int Speed { get; set; }
    public float Experience { get; set; }
    public float MaximumExperience { get; set; }

    public override string ToString()
    {
        return string.Format("[Dragon: Id={0}, Type={1}, Level={2}, Attack={3}, Defense={4}, Speed={2}, Experience={3}, MaximumExperience={4}]", 
            Id, Type, Level, Attack, Defense, Speed, Experience, MaximumExperience);
    }
}