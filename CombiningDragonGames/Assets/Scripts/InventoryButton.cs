﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryButton : MonoBehaviour
{

    SharedVariablesAcrossScenes mySVAS;

    [SerializeField]
    private Image myIcon;

    [SerializeField]
    private string buttonName;

    [SerializeField]
    private Dragon myDragon;

    //GameStatus myGameStatus;

    public void Start()
    {
        mySVAS = FindObjectOfType<SharedVariablesAcrossScenes>();
    }

    public void SetIcon(Sprite mySprite)
    {
        this.myIcon.sprite = mySprite;
    }

    public void SetDragon(Dragon dragon)
    {
        myDragon = dragon;
        //this.buttonName = myDragon.name;
    }

    public void SetCurrentDragonAcrossScenes()
    {
        mySVAS.switchDragon(myDragon);
    }

}
