﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ClickerGameLoadScenes : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene("MainScene");
        Time.timeScale = 1;
    }

    public void LoadBuyEggScene()
    {
        if (SharedVariablesAcrossScenes.myPlayer.PlayerGold >= 300)
        {
            SharedVariablesAcrossScenes.myPlayer.PlayerGold -= 300;
            SceneManager.LoadScene("BuyEggScene");
        }
    }

    public void LoadTopDownDragonShooterScene()
    {
        SceneManager.LoadScene("TopDownDragonShooterScene");
        Time.timeScale = 1;
    }

    public void LoadFallingDragonsScene()
    {
        SceneManager.LoadScene("FallingDragonsScene");
        Time.timeScale = 1;
    }

    public void loadBattleScene()
    {
        SceneManager.LoadScene("BattleScene");
    }

    public void loadMapScene()
    {
        SceneManager.LoadScene("MapScene");
    }

}
