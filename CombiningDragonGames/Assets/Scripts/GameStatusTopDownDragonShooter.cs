﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class GameStatusTopDownDragonShooter : MonoBehaviour
{

    public int points = 0;
    public TextMeshProUGUI score;
    public GameObject gameOverPanel;
    public bool gameOver = false;

    SharedVariablesAcrossScenes mySharedVariables = null;

    private void Awake()
    {
        gameOver = false;
        mySharedVariables = FindObjectOfType<SharedVariablesAcrossScenes>();
    }

    public void addScore()
    {
        points++;
        score.text = points.ToString();
    }

    public void gameOverTextAppear()
    {
        if (gameOver == false)
        {
            mySharedVariables.addAttack(points);
            gameOverPanel.transform.SetAsLastSibling();
            gameOverPanel.SetActive(true);
        }
    }
}
