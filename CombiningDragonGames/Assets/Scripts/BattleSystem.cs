﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public enum BattleState { START, PLAYERTURN, ENEMYTURN, WON, LOST }

public class BattleSystem : MonoBehaviour
{

    public BattleState state;

    public GameObject playerPrefab;
    public GameObject enemyPrefab;

    public Transform enemyBattleStation;
    public Transform playerBattleStation;

    Unit playerUnit;
    Unit enemyUnit;

    public Text dialogueText;

    public BattleHUD playerHUD;
    public BattleHUD enemyHUD;

    List<string> enemyNameList = new List<string> { "Chump Smasher", "Trogdor", "Smaug", "Captain Insano", "Shenron", "Barney" };

    // Start is called before the first frame update
    void Start()
    {
        state = BattleState.START;

        StartCoroutine(SetupBattle());
    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = Instantiate(playerPrefab, playerBattleStation);
        playerUnit = playerGO.GetComponent<Unit>();
        playerUnit.currentHP = SharedVariablesAcrossScenes.currentDragon.Level * 100;
        playerUnit.maxHP = SharedVariablesAcrossScenes.currentDragon.Level * 100;
        playerUnit.damage = Convert.ToInt32((SharedVariablesAcrossScenes.currentDragon.Level * 10) + (SharedVariablesAcrossScenes.currentDragon.Attack * 0.1));
        playerUnit.unitLevel = SharedVariablesAcrossScenes.currentDragon.Level;

        GameObject enemyGO = Instantiate(enemyPrefab, enemyBattleStation);
        enemyUnit = enemyGO.GetComponent<Unit>();
        enemyUnit.currentHP = SharedVariablesAcrossScenes.myPlayer.PlayerBossLevel * 100;
        enemyUnit.maxHP = SharedVariablesAcrossScenes.myPlayer.PlayerBossLevel * 100;
        enemyUnit.damage = SharedVariablesAcrossScenes.myPlayer.PlayerBossLevel * 10;
        //Can then decrease damage by currentDragon.Defense * 0.1 or something like that
        enemyUnit.unitLevel = SharedVariablesAcrossScenes.myPlayer.PlayerBossLevel;
        enemyUnit.unitName = enemyNameList[UnityEngine.Random.Range(0, enemyNameList.Count)];

        dialogueText.text = "A wild " + enemyUnit.unitName + " approaches...";

        playerHUD.SetHUD(playerUnit);
        enemyHUD.SetHUD(enemyUnit);

        yield return new WaitForSeconds(2f);

        state = BattleState.PLAYERTURN;
        PlayerTurn();
    }

    IEnumerator PlayerAttack()
    {
        bool isDead = enemyUnit.TakeDamage(playerUnit.damage);

        enemyHUD.SetHP(enemyUnit.currentHP);
        dialogueText.text = "The attack is successful!";


        yield return new WaitForSeconds(2f);

        if (isDead)
        {
            state = BattleState.WON;
            EndBattle();
        }
        else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }

    }

    IEnumerator EnemyTurn()
    {
        dialogueText.text = enemyUnit.unitName + " attacks!";

        yield return new WaitForSeconds(1f);

        bool isDead = playerUnit.TakeDamage(enemyUnit.damage);

        playerHUD.SetHP(playerUnit.currentHP);

        yield return new WaitForSeconds(1f);

        if (isDead)
        {
            state = BattleState.LOST;
            EndBattle();
        }
        else
        {
            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }
    }

    void EndBattle()
    {
        if(state == BattleState.WON)
        {
            SharedVariablesAcrossScenes.myPlayer.PlayerBossLevel++;
            dialogueText.text = "You won the battle!";
        } else if(state == BattleState.LOST)
        {
            dialogueText.text = "You were defeated.";
        }
    }

    void PlayerTurn()
    {
        dialogueText.text = "Choose an action:";
    }

    public void OnAttackButton()
    {
        if (state != BattleState.PLAYERTURN)
            return;

        StartCoroutine(PlayerAttack());
    }

}
