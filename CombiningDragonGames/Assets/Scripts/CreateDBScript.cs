﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class CreateDBScript : MonoBehaviour
{

    public Text DebugText;

    // Use this for initialization
    void Start()
    {
        StartSync();
    }

    private void StartSync()
    {
        var ds = new DataService("tempDatabase.db");
        ds.CreateDB();

        var dragons = ds.GetDragons();
        ToConsole(dragons);
        dragons = ds.GetFireTypeDragons();
        ToConsole("Searching for Fire type ...");
        ToConsole(dragons);
    }

    private void ToConsole(IEnumerable<Dragon> dragons)
    {
        foreach (var Dragon in dragons)
        {
            ToConsole(Dragon.ToString());
        }
    }

    private void ToConsole(string msg)
    {
        DebugText.text += System.Environment.NewLine + msg;
        Debug.Log(msg);
    }
}