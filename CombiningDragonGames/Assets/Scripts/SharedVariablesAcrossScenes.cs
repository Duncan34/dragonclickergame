﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SharedVariablesAcrossScenes : MonoBehaviour
{

    public static int speed = 100;
    public static int attack = 100;
    public static int defense = 100;
    public static int dragonId;
    public static int gold = 0;
    public static float dragonExp = 0;
    public static float dragonMaxExp = 20;
    public static int dragonLevel = 1;
    public static bool isNewGame = true;
    public static Dragon currentDragon;

    [SerializeField]
    public Sprite[] iconSprites;

    SharedVariablesAcrossScenes mySharedVariablesAcrossScenes;

    InventoryControl myInventoryControl;

    DataService ds;

    [SerializeField]
    public static List<Dragon> dragonInventory = null;

    [SerializeField]
    public static Player myPlayer = null;

    [SerializeField]
    TextMeshProUGUI expOutOfMax = null;
    [SerializeField]
    TextMeshProUGUI expMax = null;

    [SerializeField]
    GameObject dragonImageForMainScene = null;
    [SerializeField]
    Image expBarImage = null;
    [SerializeField]
    TextMeshProUGUI dragonLevelText = null;
    [SerializeField]
    TextMeshProUGUI goldText = null;
    [SerializeField]
    TextMeshProUGUI attackText = null;
    [SerializeField]
    TextMeshProUGUI defenseText = null;
    [SerializeField]
    TextMeshProUGUI speedText = null;

    private void OnApplicationQuit()
    {
        var ds = new DataService("existing.db");
        ds.SaveDragonList(dragonInventory);
        ds.SavePlayer(myPlayer);
    }

    private void OnApplicationPause(bool pause)
    {
        if(pause)
        {
            var ds = new DataService("existing.db");
            ds.SaveDragonList(dragonInventory);
            ds.SavePlayer(myPlayer);
        }
    }

    private void Awake()
    {
        mySharedVariablesAcrossScenes = FindObjectOfType<SharedVariablesAcrossScenes>();
    }

    private void Start()
    {
        loadStats();
    }

    public void fillDragonInventoryFromDB(DataService ds)
    {
        dragonInventory = new List<Dragon>();

        //var ds = new DataService("existing.db");
        var dragons = ds.GetDragons();

        foreach(Dragon d in dragons){
            dragonInventory.Add(new Dragon(d.Id, d.Type, d.Level, d.Attack, d.Defense, d.Speed, d.Experience, d.MaximumExperience));
        }

        //Set the current dragon in main scene
        switchDragon(dragonInventory[0]);

    }

    public void getMyPlayerData(DataService ds)
    {
        myPlayer = ds.GetPlayer();
    }

    public void createNewPlayer()
    {
        myPlayer = new Player(1, 1, 0, 1);
    }

    public void addDragonToListAndDatabase(Dragon d)
    {
        var ds = new DataService("existing.db");
        dragonInventory.Add(d);
        ds.CreateDragon(d.Type, d.Level, d.Attack, d.Defense, d.Speed, d.Experience, d.MaximumExperience);
        fillDragonInventoryFromDB(ds);
    }

    public void loadStats()
    {
        if (attackText != null && defenseText != null && speedText != null)
        {
            attackText.text = currentDragon.Attack.ToString();
            defenseText.text = currentDragon.Defense.ToString();
            speedText.text = currentDragon.Speed.ToString();
            dragonLevelText.text = currentDragon.Level.ToString();
            expBarImage.fillAmount = currentDragon.Experience / currentDragon.MaximumExperience;
            expOutOfMax.text = currentDragon.Experience.ToString();
            expMax.text = currentDragon.MaximumExperience.ToString();
            goldText.text = myPlayer.PlayerGold.ToString();
            setDragonImage();
        }
    }

    public void setDragonImage()
    {
        if (currentDragon.Level >= 5)
        {
            dragonImageForMainScene.GetComponent<SpriteRenderer>().sprite = iconSprites[4];
            switch (currentDragon.Type)
            {
                case "Fire":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
                    break;
                case "Grass":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(0, 255, 128);
                    break;
                case "Water":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(0, 128, 255);
                    break;
            }
        }
        else if (currentDragon.Level >= 3)
        {
            dragonImageForMainScene.GetComponent<SpriteRenderer>().sprite = iconSprites[3];
            switch (currentDragon.Type)
            {
                case "Fire":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
                    break;
                case "Grass":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(0, 255, 128);
                    break;
                case "Water":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(0, 128, 255);
                    break;
            }
        }
        else
        {
            switch (currentDragon.Type)
            {
                case "Fire":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().sprite = iconSprites[0];
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
                    break;
                case "Grass":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().sprite = iconSprites[1];
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(0, 255, 128);
                    break;
                case "Water":
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().sprite = iconSprites[2];
                    dragonImageForMainScene.GetComponent<SpriteRenderer>().color = new Color(0, 128, 255);
                    break;
            }
        }
    }

    public void addSpeed(int incomingSpeed)
    {
        currentDragon.Speed += incomingSpeed;
    }

    public void addAttack(int incomingAttack)
    {
        currentDragon.Attack += incomingAttack;
    }

    public void addDefense(int incomingDefense)
    {
        currentDragon.Defense += incomingDefense;
    }

    public void addPlayerGold()
    {
        myPlayer.PlayerGold++;
        goldText.text = myPlayer.PlayerGold.ToString();

    }

    public void addDragonExp()
    {
        currentDragon.Experience++;
        expBarImage.fillAmount = currentDragon.Experience / currentDragon.MaximumExperience;
        expOutOfMax.text = currentDragon.Experience.ToString();
        expMax.text = currentDragon.MaximumExperience.ToString();
    }

    public void dragonLevelUp()
    {
        currentDragon.Experience = 0;
        currentDragon.MaximumExperience += 50;
        currentDragon.Level++;
        dragonLevelText.text = currentDragon.Level.ToString();
        switchDragon(currentDragon);
    }

    public static void buyEgg()
    {
        myPlayer.PlayerGold -= 300;
    }

    public void switchDragon(Dragon myDragonFromButton)
    {
        currentDragon = myDragonFromButton;
        dragonId = currentDragon.Id;
        speed = currentDragon.Speed;
        attack = currentDragon.Attack;
        defense = currentDragon.Defense;
        dragonExp = currentDragon.Experience;
        dragonMaxExp = currentDragon.MaximumExperience;
        dragonLevel = currentDragon.Level;
        mySharedVariablesAcrossScenes.loadStats();
    }

    public void saveDragons()
    {
        var ds = new DataService("existing.db");
        ds.SaveDragonList(dragonInventory);
    }
}
