﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryControl : MonoBehaviour
{
    private List<Dragon> dragonInventory;

    [SerializeField]
    private GameObject buttonTemplate;

    [SerializeField]
    public Sprite[] iconSprites;

    public List<GameObject> buttons;

    GameStatus myGameStatus;

    //SharedVariablesAcrossScenes mySharedVariables;

    private void Start()
    {
        buttons = new List<GameObject>();

        myGameStatus = FindObjectOfType<GameStatus>();
        //mySharedVariables = FindObjectOfType<SharedVariablesAcrossScenes>();

        dragonInventory = SharedVariablesAcrossScenes.dragonInventory;
        //dragonInventory = myGameStatus.gameValues.dragonInventory;

        GenInventory(dragonInventory);
    }

    public void GenInventory(List<Dragon> inventoryList)
    {
        if(buttons.Count > 0)
        {
            foreach(GameObject button in buttons)
            {
                Destroy(button.gameObject);
            }
            buttons.Clear();
        }

        foreach (Dragon newDragon in inventoryList)
        {
            GameObject newButton = Instantiate(buttonTemplate) as GameObject;
            newButton.SetActive(true);
            buttons.Add(newButton);

            //create a switch case to determine dragon sprite
            switch (newDragon.Type)
            {
                case "Fire":
                    newButton.GetComponent<InventoryButton>().SetIcon(iconSprites[0]);
                    break;
                case "Grass":
                    newButton.GetComponent<InventoryButton>().SetIcon(iconSprites[1]);
                    break;
                case "Water":
                    newButton.GetComponent<InventoryButton>().SetIcon(iconSprites[2]);
                    break;
            }

            newButton.GetComponent<InventoryButton>().SetDragon(newDragon);
            newButton.transform.SetParent(buttonTemplate.transform.parent, false);
        }
    }

}
