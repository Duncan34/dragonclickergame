﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Range(0.0f, 10.0f)]
    public float moveSpeed = 7f;

    [Range(0.0f, 10.0f)]
    public float shootSpeed = 0.8f;

    public Rigidbody2D rb;
    public Camera cam;
    public Joystick joystickMove;
    public Joystick joystickLook;
    public GameObject dragon;
    public bool canShoot = true;

    GameStatusTopDownDragonShooter myGameStatusTopDownDragonShooter;

    Vector2 movement;
    Vector2 mousePos;

    // Start is called before the first frame update
    void Start()
    {
        myGameStatusTopDownDragonShooter = FindObjectOfType<GameStatusTopDownDragonShooter>();
    }

    // Update is called once per frame
    void Update()
    {
        movement.x = joystickMove.Horizontal;
        movement.y = joystickMove.Vertical;

        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        Vector2 lookDir = joystickLook.Direction;//mousePos - rb.position;
        if (lookDir.y != 0.0 || lookDir.x != 0.0)
        {
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
            rb.rotation = angle;
            if (canShoot)
            {
                StartCoroutine(TryShoot());
                //dragon.GetComponent<Shooting>().Shoot();
            }
        }
    }

    public IEnumerator TryShoot()
    {
        dragon.GetComponent<Shooting>().Shoot();
        canShoot = false;
        //wait for some time
        yield return new WaitForSeconds(shootSpeed);
        canShoot = true;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.CompareTag("enemy"))
        {
            Destroy(gameObject);
            myGameStatusTopDownDragonShooter.gameOverTextAppear();
            Time.timeScale = 0;
        }
    }
}
