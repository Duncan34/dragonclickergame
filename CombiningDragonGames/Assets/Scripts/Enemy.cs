﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Enemy : MonoBehaviour, IPointerClickHandler
{

    GameStatusFallingEnemies myGameStatusFallingEnemies;
    //EnemyMover myEnemyMover;

    public void Start()
    {
        myGameStatusFallingEnemies = FindObjectOfType<GameStatusFallingEnemies>();
        //myEnemyMover = FindObjectOfType<EnemyMover>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (myGameStatusFallingEnemies.gameOver != true) {
            myGameStatusFallingEnemies.speed += 0.2f;
            myGameStatusFallingEnemies.addScore();
            Destroy(this.gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ground"))
        {
            Destroy(this.gameObject);
            myGameStatusFallingEnemies.gameOverTextAppear();
            Time.timeScale = 0;
        }
    }

}
