﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SQLite4Unity3d;

public class Player
{

    public Player()
    {

    }

    public Player(int i, int l, int g, int b)
    {
        PlayerID = i;
        PlayerLevel = l;
        PlayerGold = g;
        PlayerBossLevel = b;
    }

    [PrimaryKey, AutoIncrement]
    public int PlayerID { get; set; }
    public int PlayerLevel { get; set; }
    public int PlayerGold { get; set; }
    public int PlayerBossLevel { get; set; }

    public override string ToString()
    {
        return string.Format("[Player: Id={0}, Level={1}, Gold={2}, BossLevel={3}]",
            PlayerID, PlayerLevel, PlayerGold, PlayerBossLevel);
    }
}
