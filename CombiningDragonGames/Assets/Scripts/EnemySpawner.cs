﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public GameObject enemyPrefab;
    public Transform enemyEmitter;
    public float spawnTime = 2f;

    private Vector2 screenBounds;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnEnemy", 2f, spawnTime);            
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnEnemy()
    {
        GameObject tempEnemy = Instantiate(enemyPrefab, enemyEmitter.position, enemyEmitter.rotation);
        tempEnemy.transform.SetParent(GameObject.FindGameObjectWithTag("TopDownCanvas").transform);
        //tempEnemy = Instantiate(enemy, enemyEmitter.transform.position, enemyEmitter.transform.rotation) as GameObject;
        //spawnTime = Random.Range(2f, 5f);
    }
}
