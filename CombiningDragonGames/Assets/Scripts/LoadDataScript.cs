﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadDataScript : MonoBehaviour
{

    SharedVariablesAcrossScenes mySharedVariables = null;

    private void Awake()
    {
        mySharedVariables = FindObjectOfType<SharedVariablesAcrossScenes>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //mySharedVariables = FindObjectOfType<SharedVariablesAcrossScenes>();

        var ds = new DataService("existing.db");

        //ds.CreateDragon("Lava", 1, 100, 100, 100, 0.0f, 100.0f);

        var dragons = ds.GetDragons();
        

        ToConsole(dragons);

        if (ds.IsDataBaseEmpty())
        {
            ds.CreateDB();
            ds.CreatePlayerDB();
            mySharedVariables.fillDragonInventoryFromDB(ds);
            mySharedVariables.createNewPlayer();
        } else
        {
            mySharedVariables.fillDragonInventoryFromDB(ds);
            mySharedVariables.getMyPlayerData(ds);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ToConsole(IEnumerable<Dragon> dragons)
    {
        foreach (var Dragon in dragons)
        {
            ToConsole(Dragon.ToString());
        }
    }

    private void ToConsole(string msg)
    {
        //DebugText.text += System.Environment.NewLine + msg;
        Debug.Log(msg);
    }
}
