﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class MainSceneController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] public Image expBarImage;
    [SerializeField] GameObject statsPanel;
    [SerializeField] GameObject storePanel;
    [SerializeField] GameObject inventory;
    [SerializeField] GameObject trainPanel;
    [SerializeField] Scrollbar inventoryScrollbar;
    //Added buttons so I can change their color in the future. Based on which is clicked I think is why I did this.
    [SerializeField] Button storeButton;
    [SerializeField] Button statsButton;
    [SerializeField] Button inventoryButton;

    [SerializeField] Sprite defaultDragonSprite;

    InventoryControl myInventoryControl;

    Dragon currentDragon;

    float currentXP;
    float maxBarXP;

    GameStatus myGameStatus;

    SharedVariablesAcrossScenes mySharedVariables = null;

    void Start()
    {
        mySharedVariables = FindObjectOfType<SharedVariablesAcrossScenes>();
        myInventoryControl = inventory.GetComponent<InventoryControl>();
        myGameStatus = FindObjectOfType<GameStatus>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        mySharedVariables.addPlayerGold();
        mySharedVariables.addDragonExp();

        if (SharedVariablesAcrossScenes.currentDragon.Experience >= SharedVariablesAcrossScenes.currentDragon.MaximumExperience)
        {
            mySharedVariables.dragonLevelUp();
        }

    }

    public void ToggleStatsPanel()
    {
        statsPanel.SetActive(!statsPanel.activeSelf);

        if (statsPanel.activeSelf)
        {
            storePanel.SetActive(false);
            inventory.SetActive(false);
            trainPanel.SetActive(false);
        }
    }

    public void ToggleStorePanel()
    {
        storePanel.SetActive(!storePanel.activeSelf);

        if (storePanel.activeSelf)
        {
            statsPanel.SetActive(false);
            inventory.SetActive(false);
            trainPanel.SetActive(false);
        }
    }

    public void ToggleInventory()
    {
        inventory.SetActive(!inventory.activeSelf);

        if (inventory.activeSelf)
        {
            storePanel.SetActive(false);
            statsPanel.SetActive(false);
            trainPanel.SetActive(false);
        }
    }

    public void ToggleTrain()
    {
        trainPanel.SetActive(!trainPanel.activeSelf);

        if (trainPanel.activeSelf)
        {
            storePanel.SetActive(false);
            statsPanel.SetActive(false);
            inventory.SetActive(false);
        }
    }

}
