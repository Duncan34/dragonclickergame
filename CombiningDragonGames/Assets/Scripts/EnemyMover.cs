﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{

    //[Range(0.0f, 20.0f)]
    //public float speed = 1.0f;

    GameStatusFallingEnemies myGameStatusFallingEnemies;

    // Start is called before the first frame update
    void Start()
    {
        myGameStatusFallingEnemies = FindObjectOfType<GameStatusFallingEnemies>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.down * myGameStatusFallingEnemies.speed * Time.deltaTime;
    }
}
