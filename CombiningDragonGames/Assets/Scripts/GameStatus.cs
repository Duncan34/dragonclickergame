﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;
using System;
//using CodeMonkey;

public class GameValues
{
    public List<Dragon> dragonInventory;
    public int numberOfDragons = 0;
    public int gold = 0;
}

public class GameStatus : MonoBehaviour
{

    [SerializeField] Sprite defaultDragonSprite;
    public GameValues gameValues = new GameValues();

}
