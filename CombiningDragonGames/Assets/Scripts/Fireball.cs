﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Fireball : MonoBehaviour
{

    public GameObject hitEffect;
    GameStatusTopDownDragonShooter myGameStatusTopDownDragonShooter;

    //private void Start()
    private void Awake()
    {
        myGameStatusTopDownDragonShooter = FindObjectOfType<GameStatusTopDownDragonShooter>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("enemy"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(effect, 1f);
            myGameStatusTopDownDragonShooter.addScore();
        } else if (collision.gameObject.CompareTag("Player") != true)
        {
            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(effect, 1f);
            Destroy(gameObject);
        }
    }

}
