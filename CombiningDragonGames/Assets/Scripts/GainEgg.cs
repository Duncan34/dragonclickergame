﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GainEgg : MonoBehaviour
{

    [SerializeField] GameObject blockSparklesVFX;
    [SerializeField] Sprite[] dragonEggSprites;
    [SerializeField] ClickerGameLoadScenes mySceneLoader;

    GameStatus myGameStatus;

    Dragon newDragon;

    SpriteRenderer mySprite;

    SharedVariablesAcrossScenes mySharedVariables = null;
    DataService ds = null;

    private void Awake()
    {
        mySharedVariables = FindObjectOfType<SharedVariablesAcrossScenes>();
        myGameStatus = FindObjectOfType<GameStatus>();
        mySprite = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Use this to save current dragons before inserting a new one and reloading the data. Otherwise will lose current exp etc.
        mySharedVariables.saveDragons();

        string Type = "";
        TriggerSparklesVFX();
        int arrayIndex = Random.Range(0, dragonEggSprites.Length);
        mySprite.sprite = dragonEggSprites[arrayIndex];

        if(arrayIndex == 0)
        {
            Type = "Fire";
        }
        else if (arrayIndex == 1)
        {
            Type = "Grass";
        }
        else if (arrayIndex == 2)
        {
            Type = "Water";
        }
        //myGameStatus.CreateNewDragon(mySprite.sprite);
        mySharedVariables.addDragonToListAndDatabase(new Dragon(Type, 1, 100, 100, 100, 0, 100));
    }

    private void TriggerSparklesVFX()
    {
        GameObject sparkles = Instantiate(blockSparklesVFX, transform.position, transform.rotation);
        Destroy(sparkles, 5f);
    }

    public void GoBack()
    {
        mySceneLoader.LoadMainScene();
    }
}
