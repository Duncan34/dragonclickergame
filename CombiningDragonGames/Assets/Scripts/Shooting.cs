﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{

    public Transform firePoint;
    public GameObject fireballPrefab;

    public float fireBallForce = 5f;

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetButtonDown("Fire1"))
        //{
        //    Shoot();
        //}
    }

    public void Shoot()
    {
        GameObject fireBall = Instantiate(fireballPrefab, firePoint.position, (firePoint.rotation * Quaternion.Euler(0, 0, 90)));

        Rigidbody2D rb = fireBall.GetComponent<Rigidbody2D>();

        rb.AddForce(firePoint.up * fireBallForce, ForceMode2D.Impulse);
    }
}
