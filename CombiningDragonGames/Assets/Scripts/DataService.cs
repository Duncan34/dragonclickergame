﻿using SQLite4Unity3d;
using UnityEngine;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

public class DataService
{

    private SQLiteConnection _connection;

    public DataService(string DatabaseName)
    {

#if UNITY_EDITOR
        var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		
#elif UNITY_STANDALONE_OSX
		var loadDb = Application.dataPath + "/Resources/Data/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif
        _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        Debug.Log("Final PATH: " + dbPath);

    }

    public void CreateDB()
    {
        _connection.DropTable<Dragon>();
        _connection.CreateTable<Dragon>();

        _connection.InsertAll(new[]{
            new Dragon{
                Id = 1,
                Type = "Fire",
                Level = 1,
                Attack = 100,
                Defense = 100,
                Speed = 100,
                Experience = 0,
                MaximumExperience = 100
            }
        });
    }

    public IEnumerable<Dragon> GetDragons()
    {
        return _connection.Table<Dragon>();
    }

    public bool IsDataBaseEmpty()
    {
        return (_connection.Table<Dragon>().Count() <= 0);
    }

    public IEnumerable<Dragon> GetFireTypeDragons()
    {
        return _connection.Table<Dragon>().Where(x => x.Type == "Fire");
    }

    public Dragon GetOneWaterTypeDragon()
    {
        return _connection.Table<Dragon>().Where(x => x.Type == "Water").FirstOrDefault();
    }

    public Dragon CreateDragon(string myType, int myLevel, int myAttack, int myDefense, int mySpeed, float myExperience, float myMaxExperience)
    {
        var p = new Dragon
        {
            Type = myType,
            Level = myLevel,
            Attack = myAttack,
            Defense = myDefense,
            Speed = mySpeed,
            Experience = myExperience,
            MaximumExperience = myMaxExperience
        };
        _connection.Insert(p);
        return p;
    }

    public void SaveDragonList(List<Dragon> myDragonListToSave)
    {
        //_connection.InsertAll(myDragonListToSave);
        _connection.UpdateAll(myDragonListToSave);
    }

    //********************************************************************
    //
    // This is the player section
    //
    //********************************************************************

    public void CreatePlayerDB()
    {
        _connection.DropTable<Player>();
        _connection.CreateTable<Player>();

        _connection.InsertAll(new[]{
            new Player{
                PlayerID = 1,
                PlayerLevel = 1,
                PlayerGold = 0,
                PlayerBossLevel = 1
            }
        });
    }

    public Player GetPlayer()
    {
        //return _connection.Table<Player>().Where(x => x.PlayerID == 1).FirstOrDefault();
        return _connection.Table<Player>().First();
            //.Select<Player>().First;
    }

    public bool IsPlayerDataBaseEmpty()
    {
        return (_connection.Table<Player>().Count() <= 0);
    }

    public void CreatePlayer()
    {
        var p = new Player
        {
            PlayerID = 1,
            PlayerLevel = 1,
            PlayerGold = 0,
            PlayerBossLevel = 1
        };
        _connection.Insert(p);
    }

    public void SavePlayer(Player myPlayerSave)
    {
        _connection.Update(myPlayerSave);
    }
}