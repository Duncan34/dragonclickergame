﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingEnemySpawner : MonoBehaviour
{
    [SerializeField]
    private float xLimit = 0.0f;
    [SerializeField]
    private float[] xPositions = null;
    [SerializeField]
    private GameObject[] enemyPrefabs = null;
    [SerializeField]
    private Wave[] wave = null;
    [SerializeField]
    private Transform parent = null;

    private float currentTime = 0.0f;

    List<float> remainingPositions = new List<float>();
    private int waveIndex = 0;

    float xPos = 0;
    int rand = 0;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = 0;
        remainingPositions.AddRange(xPositions);
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= Time.deltaTime;
        if (currentTime <= 0)
        {
            SelectWave();
        }
    }

    void SpawnEnemy(float xPos)
    {
        int r = Random.Range(0, 4); //Second number is types of enemies
        GameObject enemyObj = Instantiate(enemyPrefabs[r], new Vector3(xPos, transform.position.y, 0), Quaternion.identity);
        enemyObj.transform.SetParent(parent, false);
    }

    void SelectWave()
    {
        remainingPositions = new List<float>();
        remainingPositions.AddRange(xPositions);

        waveIndex = Random.Range(0, wave.Length);

        currentTime = wave[waveIndex].delayTime;

        if (wave[waveIndex].spawnAmount == 1)
        {
            xPos = Random.Range(-xLimit, xLimit);
        }
        else if (wave[waveIndex].spawnAmount > 1)
        {
            rand = Random.Range(0, remainingPositions.Count);
            xPos = remainingPositions[rand];
            remainingPositions.RemoveAt(rand);
        }

        for (int i = 0; i < wave[waveIndex].spawnAmount; i++)
        {
            SpawnEnemy(xPos);
            rand = Random.Range(0, remainingPositions.Count);
            xPos = remainingPositions[rand];
            remainingPositions.RemoveAt(rand);
        }
    }

}

[System.Serializable]
public class Wave
{
    public float delayTime;
    public float spawnAmount;
}
