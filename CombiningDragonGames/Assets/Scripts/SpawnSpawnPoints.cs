﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSpawnPoints : MonoBehaviour
{

    public GameObject spawnPointPrefab;

    RectTransform rectTransform;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();

        float width = rectTransform.sizeDelta.x;
        float height = rectTransform.sizeDelta.y;

        InvokeRepeating("SpawnSpawnPoint", 5f, 5f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnSpawnPoint()
    {

        float width = rectTransform.sizeDelta.x;
        float height = rectTransform.sizeDelta.y;
        //Set random values
        Vector3 spawn;
        //spawn.x = Random.Range(width * -1, width);
        //spawn.y = Random.Range(height * -1, height);

        spawn.x = (Random.Range(0, Screen.width));
        spawn.y = (Random.Range(0, Screen.height));
        spawn.z = 99;//(Random.Range(-14, 0));

        //set these as spawn positions
        GameObject tempSpawner = Instantiate(spawnPointPrefab, Camera.main.ScreenToWorldPoint(spawn), transform.rotation);
        RectTransform rt = tempSpawner.GetComponent<RectTransform>();
        rt.localScale = new Vector3(.01f, .01f);
        //tempSpawner.transform.SetParent(FindObjectOfType<Canvas>().transform);
        tempSpawner.transform.SetParent(GameObject.FindGameObjectWithTag("TopDownCanvas").transform);
        //GameObject tempSpawner = Instantiate(spawnPointPrefab, new Vector3(Camera.main.ScreenToViewportPoint(Random.Range(0, Screen.width)), Camera.main.ScreenToViewportPoint(Random.Range(0, Screen.height)), Random.Range(-14, 0)), Quaternion.identity) as GameObject;
        //tempEnemy = Instantiate(enemy, enemyEmitter.transform.position, enemyEmitter.transform.rotation) as GameObject;
        //spawnTime = Random.Range(2f, 5f);
    }
}
